---
layout: markdown_page
title: "Section Vision - <SECTION-NAME>"
---

- TOC
{:toc}

## <SECTION-NAME> Overview
<!-- Provide a general overview of the section, what is covered within it and introduce themes.
Include details on our current market share (if available), the total addressable market (TAM), our competitive position, and high level feedback from customers on current features -->

## Challenges
<!-- What are our constraints?  (team size, product maturity, lack of brand, GTM challenges, etc). What are our market/competitive challenges? -->

### <THEME-ONE>
<!-- Introduce theme, stages and categories it applies to and reference liberally. Highlight
important features, epics and issues from that theme specifically. Concentrate not on what we will do
but also what we won't do over the next 12 months. -->

### <THEME-TWO>
<!-- Introduce theme, stages and categories it applies to and reference liberally. Highlight
important features, epics and issues from that theme specifically. Concentrate not on what we will do
but also what we won't do over the next 12 months. -->

### <THEME-THREE>
<!-- Introduce theme, stages and categories it applies to and reference liberally. Highlight
important features, epics and issues from that theme specifically. Concentrate not on what we will do
but also what we won't do over the next 12 months. -->

## Foundation
<!-- Provide a transition from the themes to What's Next by providing an overview of how we will pursue the themes. -->

## 3 Year Strategy
<!-- What does the landscape look like in three years? -->

## What's Next
<!-- Conclude with What must we get done in the next 12 months? What won't be done? Then
transition into direction items highlighting those themes across the relevant stages -->

<%= direction %>